package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.IntPredicate;

public class GitRepository {
	public String headPath;
	public String masterPath;
	public String path;
	
	public GitRepository(String path) {
		this.path=new String(path);	
	}
		
	public String getHeadRef() {
		headPath=this.path+"/HEAD";
		
		try(BufferedReader bf=new BufferedReader(new FileReader(headPath))){
			return bf.readLine().substring(5);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				}
		return "";
		}

	public String getRefHash(String string) {
		
		masterPath=new String(this.path+"/"+string);
		
		try(BufferedReader bf=new BufferedReader(new FileReader(masterPath))){
			return bf.readLine();
			
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				}
		return "";
	}
	
}
